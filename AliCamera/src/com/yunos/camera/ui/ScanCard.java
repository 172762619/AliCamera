package com.yunos.camera.ui;

import java.util.List;

import org.apache.http.util.EncodingUtils;
import org.json.JSONObject;
import org.json.JSONTokener;

import a_vcard.android.syncml.pim.PropertyNode;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.way.camera.R;
import com.yunos.camera.Util;
import com.yunos.camera.network.NetworkUtil;
import com.yunos.camera.qrcode.DecodeHandler;
import com.yunos.camera.qrcode.VcardUtils;

public class ScanCard extends LinearLayout{

    private static final int TYPE_GOODS = 0;
    private static final int TYPE_EXPRESS = 1;
    private static final int TYPE_COPY = 2;
    private static final int TYPE_APP = 3;
    private static final int TYPE_BROWSER = 5;

    private static final String BUTTON_GOODS = "goods";
    private static final String BUTTON_EXPRESS = "express";
    private static final String BUTTON_COPY = "copy";
    private static final String BUTTON_APP = "app";
    private static final String BUTTON_BROWSER = "browser";

    public interface ActionListener {
        void cancel();
        void copyText(String content);
        void addContact();
        void openBrowser(String url);
        void openBarcodeInBrowser();
        void setResultAndFinish();
        void clickInOnlineView(String pkgName, String appData, String webUrl);
        void shareContent();
        void jumpAppOffline(String appName);
        void jumpAppOnline(String pkgName, String data);
    }

    public static final int TYPE_TEXT = 0;
    public static final int TYPE_VCARD = 1;
    public static final int TYPE_LINK = 2;
    public static final int TYPE_JUMP = 3;
    public static final int TYPE_BARCODE_ERROR = 4;
    public static final int TYPE_INVOCATION = 5;

    // global handles
    private LayoutInflater mInflator;
    private Context mContext;
    private ActionListener mListener;
    private Resources mResources;

    // temporal variables
    private List<PropertyNode> mVcardInfo;
    private String mScanResult;
    private String mCopyContent;
    private View mOfflineView;
    private WebView mOnlineView;
    private long mRequestTime;
    private boolean mRequstedStarted = false;
    private int mCodeType = DecodeHandler.RESULT_NONE;
    private String mJumpType = null;
    private String mJumpPkgName = null;
    private String mJumpData = null;
    private String mJumpAppName = null;
    private String mLinkUrl = null;
    String laiwangPrefix = "http://laiwang.com";
    String alipayPrefix = "https://qr.alipay.com";

    private ProgressDialog mProgressDialog;
    // Header
    private TextView mTypeTextView;
    // Title Panel
    private TextView mTitleTextView;
    private TextView mContentTextView;
    private ImageView mIconImageView;

    // Content Panel
    private LinearLayout mContentContainer;
    private TextView mErrorTextView;

    // Button Panel
    private TextView mCanelButton;
    private TextView mActionButton;
    private View mButtonDivider;

    public ScanCard(Context context) {
        super(context);
        init(context);
    }

    public ScanCard(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ScanCard(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        mInflator = ((Activity)mContext).getLayoutInflater();
        mResources = mContext.getResources();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mTypeTextView = (TextView) findViewById(R.id.type);
        mTitleTextView = (TextView) findViewById(R.id.title);
        mContentTextView = (TextView) findViewById(R.id.content);
        mIconImageView = (ImageView) findViewById(R.id.icon);
        mContentContainer = (LinearLayout) findViewById(R.id.container);
        mErrorTextView = (TextView) findViewById(R.id.error);
        mOfflineView = findViewById(R.id.localcontent);
        mOnlineView = (WebView) findViewById(R.id.webcontent);
        mOnlineView.setWebViewClient(mWebViewClient);
        mActionButton = (TextView) findViewById(R.id.right_button);
        mCanelButton = (TextView) findViewById(R.id.left_button);
        mCanelButton.setOnClickListener(mCancelListener);
        mButtonDivider = findViewById(R.id.divider);
        resetScanCard();
    }

    public void setActionListener(ActionListener action) {
        mListener = action;
    }

    public void setCodeType(int type) {
        mCodeType = type;
    }

    public void setVCarddded() {
        mActionButton.setText(mResources.getString(R.string.scan_card_button_added));
        mActionButton.setTextColor(mResources
                .getColor(R.color.scan_card_action_button_selected_color));
        mActionButton.setEnabled(false);
    }

    public void setTextCopied() {
        mActionButton.setEnabled(false);
        mActionButton.setTextColor(mResources
                .getColor(R.color.scan_card_action_button_selected_color));
        mActionButton.setText(mResources.getString(R.string.scan_card_button_copied));
    }

    private void showLink(String link) {
        // Update UI
        updateHeader(mResources.getString(R.string.scan_card_header_qrcode));
        updateTitle(mResources.getString(R.string.scan_card_title_name));
        updateIcon(R.drawable.ic_camera_recognition_url);
        updateActionButton(mResources.getString(R.string.scan_card_button_open), mLinkListener);
        updateCancelButton();
        updateContent(mResources.getString(R.string.scan_card_content_unknown));
        // Update ScroolView
        mContentContainer.removeAllViews();
        RelativeLayout iItem = (RelativeLayout) mInflator.inflate(R.layout.scan_card_item, null);
        ((TextView)iItem.findViewById(R.id.title)).setText(mResources.getString(R.string.scan_card_title_url));
        ((TextView)iItem.findViewById(R.id.content)).setText(link);
        mContentContainer.addView(iItem);
        mLinkUrl = link;
    }

    private OnClickListener mAddContactListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.addContact();
        }
    };

    private OnClickListener mOfflineJumpListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.jumpAppOffline(mJumpAppName);
        }
    };

    private OnClickListener mOnlineJumpListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.jumpAppOnline(mJumpPkgName, mJumpData);
        }
    };

    private OnClickListener mLinkListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.openBrowser(mLinkUrl);
        }
    };

    private OnClickListener mBarcodeListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.openBarcodeInBrowser();
        }
    };

    private OnClickListener mCopyListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.copyText(mCopyContent);
        }
    };

    private OnClickListener mReturnListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.setResultAndFinish();
        };
    };

    private OnClickListener mCancelListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.cancel();
        }
    };

    private OnClickListener mShareListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            mListener.shareContent();
        }
    };

    private void prepareOfflineCard() {
        mOfflineView.setVisibility(View.VISIBLE);
        mOnlineView.setVisibility(View.GONE);
    }

    private void prepareOnlineCard() {
        mOnlineView.setVisibility(View.VISIBLE);
        mOfflineView.setVisibility(View.GONE);
    }

    public void resetScanCard() {
        mErrorTextView.setVisibility(View.GONE);
        updateActionButton("", null);
        mOnlineView.setVisibility(View.GONE);
        mOfflineView.setVisibility(View.GONE);
        mOnlineView.loadUrl("about:blank");
        mOnlineView.stopLoading();
        mActionButton.setTextColor(mResources.getColor(R.color.scan_card_action_button_color));
        mActionButton.setEnabled(false);
        mCanelButton.setText(null);
        mCanelButton.setEnabled(false);
        mButtonDivider.setVisibility(View.GONE);
        mCopyContent = null;
        mScanResult = null;
        mJumpData = null;
        mJumpType = null;
        mJumpPkgName = null;
        mJumpAppName = null;
        mCodeType = DecodeHandler.RESULT_NONE;
        mRequstedStarted = false;
    }

    private static final int MSG_CHECK_BAR_TIMEOUT = 0;
    private static final int MSG_CHECK_QR_TIMEOUT = 1;
    private static final int MSG_SHOW_SCAN_CARD = 2;
    private static final int MSG_SHOW_BUTTONS = 3;
    private static final int TIMEOUT = 5000;

    private Handler mHander = new Handler() {
        public void handleMessage(android.os.Message msg) {
            Log.v("mk", "WebView, handleMessage(), msg.what = " + msg.what);
            if (msg.what == MSG_CHECK_BAR_TIMEOUT) {
                mRequstedStarted = false;
                mOnlineView.stopLoading();
                showCodeError(mScanResult);
                Util.fadeIn(ScanCard.this);
            } else if (msg.what == MSG_CHECK_QR_TIMEOUT) {
                mRequstedStarted = false;
                mOnlineView.stopLoading();
                if (mScanResult != null && (mScanResult.contains(laiwangPrefix) || mScanResult.contains(alipayPrefix))) {
                    showJump(mScanResult);
                } else {
                    showLink(mScanResult);
                }
                prepareOfflineCard();
                Util.fadeIn(ScanCard.this);
            } else if (msg.what == MSG_SHOW_SCAN_CARD) {
                // Show Scan Card
                // Util.fadeIn(ScanCard.this);
                ScanCard.this.setVisibility(View.VISIBLE);
                Log.v("WebView", "handler, show card, time = " + (System.currentTimeMillis() - mRequestTime));
            } else if (msg.what == MSG_SHOW_BUTTONS) {
                Log.v("mk", "MSG_SHOW_BUTTONS, arg1 = " + msg.arg1);
                switch (msg.arg1) {
                    case TYPE_GOODS:
                        updateActionButton(mResources.getString(R.string.scan_card_button_share), mShareListener);
                        break;
                    case TYPE_EXPRESS:
                        updateActionButton(mResources.getString(R.string.scan_card_button_more), mBarcodeListener);
                        break;
                    case TYPE_COPY:
                        updateActionButton(mResources.getString(R.string.scan_card_button_copy), mCopyListener);
                        break;
                    case TYPE_APP:
                        updateActionButton(mResources.getString(R.string.scan_card_button_jump), mOnlineJumpListener);
                        break;
                    case TYPE_BROWSER:
                        updateActionButton(mResources.getString(R.string.scan_card_button_open), mLinkListener);
                        break;
                    default:
                        break;
                }
                updateCancelButton();
            }
        };
    };

    private void updateHeader(String name) {
        mTypeTextView.setText(name);
    }

    private void updateTitle(String name) {
        mTitleTextView.setText(name);
    }

    private void updateContent(String name) {
        mContentTextView.setText(name);
    }

    private void updateIcon(int resId) {
        mIconImageView.setImageResource(resId);
    }

    private void updateActionButton(String name, View.OnClickListener listener) {
        mActionButton.setText(name);
        mActionButton.setEnabled(true);
        mActionButton.setOnClickListener(listener);
    }

    private void updateCancelButton() {
        mCanelButton.setText(mResources.getString(R.string.scan_card_button_cancel));
        mCanelButton.setEnabled(true);
        mButtonDivider.setVisibility(View.VISIBLE);
    }

    public void updateOfflineCard(int type, String result) {
        mScanResult = result;
        prepareOfflineCard();
        switch (type) {
            case TYPE_TEXT:
                showText(result);
                break;
            case TYPE_VCARD:
                showVcard(result);
                break;
            case TYPE_LINK:
                showLink(result);
                break;
            case TYPE_JUMP:
                showJump(result);
                break;
            case TYPE_BARCODE_ERROR:
                showCodeError(result);
                break;
            case TYPE_INVOCATION:
                showResult(result);
                break;
            default:
                break;
        }
    }

    public void updateOnlineCard(String result, String postData) {
        mScanResult = result;
        mCopyContent = mScanResult;
        prepareOnlineCard();
        switch (mCodeType) {
            case DecodeHandler.RESULT_BAR:
                showBarCodeView(result, postData);
                break;
            case DecodeHandler.RESULT_QR:
                showQRCodeView(result, postData);
                break;
            default:
                break;
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void showQRCodeView(String result, String postData) {
        updateHeader(mResources.getString(R.string.scan_card_header_qrcode));
        mRequestTime = System.currentTimeMillis();
        Log.v("WebView", "before post qr code url to webview, time = 0");
        mRequstedStarted = true;
        mOnlineView.postUrl(NetworkUtil.URL_BARCODE, EncodingUtils.getBytes(postData, "base64"));
        mOnlineView.getSettings().setJavaScriptEnabled(true);
        if (mQRCodeJsObject == null) {
            mQRCodeJsObject = new QRCodeJsObject();
        }
        mOnlineView.addJavascriptInterface(mQRCodeJsObject, "demo");
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void showBarCodeView(String result, String postData) {
        updateHeader(mResources.getString(R.string.scan_card_header_barcode));
        mRequestTime = System.currentTimeMillis();
        Log.v("WebView", "before post bar code url to webview, time = 0");
        mRequstedStarted = true;
        mOnlineView.loadUrl(NetworkUtil.URL_BARCODE + "?" + postData);
        mOnlineView.getSettings().setJavaScriptEnabled(true);
        if (mBarCodeJsObject == null) {
            mBarCodeJsObject = new BarCodeJsObject();
        }
        mOnlineView.addJavascriptInterface(mBarCodeJsObject, "demo");
    }

    private void showVcard(String result) {
        updateHeader(mResources.getString(R.string.scan_card_header_qrcode));
        updateIcon(R.drawable.ic_camera_recognition_vcard);
        updateActionButton(mResources.getString(R.string.scan_card_button_add_contact), mAddContactListener);
        updateCancelButton();
        mVcardInfo = VcardUtils.getContactInfo(result);
        String title = null;
        String content = null;
        mContentContainer.removeAllViews();
        Log.v("mk", "contact.length ======== " + mVcardInfo.size());
        for (PropertyNode item : mVcardInfo) {
            Log.v("mk", "item propName = " + item.propName + ", propValue = " + item.propValue);
            if (item.propName.equals(VcardUtils.VCARD_NAME)) {
                content = item.propValue.replaceAll(";", "");
                mTitleTextView.setText(mResources.getString(R.string.scan_card_title_vcard_name));
                mContentTextView.setText(content);
                continue;
            } else if (item.propName.equals(VcardUtils.VCARD_TEL)) {
                title = mResources.getString(R.string.scan_card_title_vcard_telephoe);
                content = item.propValue.replaceAll(";", "");
            } else if (item.propName.equals(VcardUtils.VCARD_EMAIL)) {
                title = mResources.getString(R.string.scan_card_title_vcard_email);
                content = item.propValue.replaceAll(";", "");
            } else if (item.propName.equals(VcardUtils.VCARD_URL)) {
                title = mResources.getString(R.string.scan_card_title_vcard_homepage);
                content = item.propValue.replaceAll(";", "");
            } else if (item.propName.equals(VcardUtils.VCARD_ADR)) {
                title = mResources.getString(R.string.scan_card_title_vcard_address);
                content = item.propValue.replaceAll(";", "");
            }
            Log.v("mk", "title = " + title + ", content = " + content);
            if (title != null && content != null && !title.equalsIgnoreCase("") && !content.equalsIgnoreCase("")) {
                RelativeLayout iItem = (RelativeLayout) mInflator.inflate(R.layout.scan_card_item, null);
                ((TextView)iItem.findViewById(R.id.title)).setText(title);
                ((TextView)iItem.findViewById(R.id.content)).setText(content);
                mContentContainer.addView(iItem);
                title = null;
                content = null;
            }
        }
    }

    private void showText(String text) {
        // Update UI
        updateHeader(mResources.getString(R.string.scan_card_header_qrcode));
        updateTitle(mResources.getString(R.string.scan_card_title_content));
        updateIcon(R.drawable.ic_camera_recognition_text);
        updateContent(mResources.getString(R.string.scan_card_title_text));
        updateActionButton(mResources.getString(R.string.scan_card_button_copy), mCopyListener);
        updateCancelButton();
        // Update Text View
        mContentContainer.removeAllViews();
        RelativeLayout item = (RelativeLayout) mInflator.inflate(R.layout.scan_card_item_text, null);
        mCopyContent = text;
        ((TextView)item.findViewById(R.id.text)).setText(text);
        mContentContainer.addView(item);
    }

    private void showResult(String text) {
        // Update UI
        if (mCodeType == DecodeHandler.RESULT_BAR) {
            updateHeader(mResources.getString(R.string.scan_card_header_barcode));
        } else if (mCodeType == DecodeHandler.RESULT_QR) {
            updateHeader(mResources.getString(R.string.scan_card_header_qrcode));
        } else {
            throw new RuntimeException();
        }
        updateIcon(R.drawable.ic_camera_recognition_text);
        updateTitle(mResources.getString(R.string.scan_card_title_content));
        updateContent(mResources.getString(R.string.scan_card_title_text));
        updateActionButton(mResources.getString(android.R.string.ok), mReturnListener);
        updateCancelButton();
        // Update Text View
        mContentContainer.removeAllViews();
        RelativeLayout item = (RelativeLayout) mInflator.inflate(R.layout.scan_card_item_text, null);
        ((TextView)item.findViewById(R.id.text)).setText(text);
        mContentContainer.addView(item);
    }

    private void showJump(String text) {
        updateHeader(mResources.getString(R.string.scan_card_header_qrcode));
        updateTitle(mResources.getString(R.string.scan_card_title_name));
        updateIcon(R.drawable.ic_camera_recognition_url);
        updateActionButton(mResources.getString(R.string.scan_card_button_jump), mOfflineJumpListener);
        updateCancelButton();
        String content = null;
        if (text.contains(alipayPrefix)) {
            mJumpAppName = "alipay";
            content = mResources.getString(R.string.scan_card_jump_alipay);
        } else if (text.contains(laiwangPrefix)) {
            mJumpAppName = "laiwang";
            content = mResources.getString(R.string.scan_card_jump_laiwang);
        }
        updateContent(content);
        // Update ScroolView
        mContentContainer.removeAllViews();
        RelativeLayout iItem = (RelativeLayout) mInflator.inflate(R.layout.scan_card_item, null);
        ((TextView)iItem.findViewById(R.id.title)).setText(mResources.getString(R.string.scan_card_title_url));
        ((TextView)iItem.findViewById(R.id.content)).setText(text);
        mContentContainer.addView(iItem);
    }

    private void showCodeError(String text) {
        prepareOfflineCard();
        mErrorTextView.setVisibility(View.VISIBLE);
        String errMsg = mResources.getString(R.string.scan_card_error);
        mErrorTextView.setText(errMsg);
        // Update UI
        Log.v("mk", "mCodeType = " + mCodeType);
        if (mCodeType == DecodeHandler.RESULT_QR) {
            updateHeader(mResources.getString(R.string.scan_card_header_qrcode));
        } else if (mCodeType == DecodeHandler.RESULT_BAR) {
            updateHeader(mResources.getString(R.string.scan_card_header_barcode));
        }
        updateIcon(R.drawable.ic_camera_recognition_unsafe);
        updateTitle(mResources.getString(R.string.scan_card_title_name));
        updateContent(mResources.getString(R.string.scan_card_unknown_code));
        updateActionButton(mResources.getString(R.string.scan_card_button_copy), mCopyListener);
        updateCancelButton();
        mCopyContent = text;
        // Update Text View
        mContentContainer.removeAllViews();
        RelativeLayout item = (RelativeLayout) mInflator.inflate(R.layout.scan_card_item_text, null);
        ((TextView)item.findViewById(R.id.title)).setText(mResources.getString(R.string.scan_card_code_info));
        ((TextView)item.findViewById(R.id.text)).setText(mScanResult);
        mContentContainer.addView(item);
    }

    private class BarCodeJsObject {

        @JavascriptInterface
        public void getActionFromWebview(String param) {
            if (param != null) {
                JSONTokener jsonParser = new JSONTokener(param);
                try {
                    JSONObject json = (JSONObject) jsonParser.nextValue();
                    String pkgName = json.getString("packageName");
                    String appData = json.getString("appData");
                    String webUrl = json.getString("webUrl");
                    mListener.clickInOnlineView(pkgName, appData, webUrl);
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                } finally {
                    jsonParser = null;
                }
            }
        }

        @JavascriptInterface
        public void updateButton(String param) {
            Log.v("WebView", "updateButton(), time = " + (System.currentTimeMillis() - mRequestTime));
            Log.v("mk", "param = " + param);
            JSONTokener jsonParser = new JSONTokener(param);
            String type = null;
            try {
                JSONObject json = (JSONObject) jsonParser.nextValue();
                type= json.getString("type");
                Log.v("mk", "type = " + type);
            } catch (Exception e) {
                e.printStackTrace();
                showCodeError(mScanResult);
                return;
            } finally {
                jsonParser = null;
            }
            mHander.obtainMessage(MSG_SHOW_BUTTONS, getType(type), 0).sendToTarget();
        }
    };

    private class QRCodeJsObject {

        @JavascriptInterface
        public void getActionFromWebview(String param) {
            // no action currently
        }

        @JavascriptInterface
        public void updateButton(String param) {
            Log.v("mk", "param = " + param);
            JSONTokener jsonParser = new JSONTokener(param);
            try {
                JSONObject json = (JSONObject) jsonParser.nextValue();
                String typearray[]= json.getString("type").split("-");
                mJumpType = typearray[0];
                mJumpPkgName = typearray[1];
                mJumpData = json.getString("data");
            } catch (Exception e) {
                e.printStackTrace();
                showCodeError(mScanResult);
                return;
            } finally {
                jsonParser = null;
            }
            if (BUTTON_BROWSER.equals(mJumpType)) {
                mLinkUrl = mJumpData;
            }
            mHander.obtainMessage(MSG_SHOW_BUTTONS, getType(mJumpType), 0).sendToTarget();
        }
    };

    private BarCodeJsObject mBarCodeJsObject = null;
    private QRCodeJsObject mQRCodeJsObject = null;

    private WebViewClient mWebViewClient = new WebViewClient() {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.v("WebView", "onPageStarted(), time = " + (System.currentTimeMillis() - mRequestTime));
            Log.v("WebView", "onPageStarted(), url = " + url.toString());
            mOnlineView.getSettings().setBlockNetworkImage(true);
            if (mRequstedStarted) {
                sendTimeOutMessage();
                mHander.sendEmptyMessage(MSG_SHOW_SCAN_CARD);
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            Log.v("WebView", "onPageFinished(), time = " + (System.currentTimeMillis() - mRequestTime));
            Log.v("WebView", "onPageFinished(), url = " + url.toString());
            if (mRequstedStarted) {
                removeTimeOutMessage();
            }
            mOnlineView.getSettings().setBlockNetworkImage(false);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description,
                String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            Log.v("WebView", "onReceivedError(), errorCode = " + errorCode + ", failingUrl = " + failingUrl + ", time = " + (System.currentTimeMillis() - mRequestTime));
            if (mRequstedStarted) {
                removeTimeOutMessage();
                mRequstedStarted = false;
            }
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.v("mk", "WebView shouldOverrideUrlLoading(), url = " + url.toString());
            if (url.startsWith("http://cameraapi.yunos.com") || url.startsWith("https://cameraapi.yunos.com")) {
                return false;
            } else {
                sendTimeOutMessage();
                return true;
            }
        }

        public void onLoadResource(WebView view, String url) {
            Log.v("mk", "WebView onLoadResource(), url = " + url.toString() + ", time = " + (System.currentTimeMillis() - mRequestTime));
            if (mRequstedStarted) {
                removeTimeOutMessage();
            }
        };

    };

    private int getType(String type) {
        if (BUTTON_GOODS.equals(type)) {
            return TYPE_GOODS;
        } else if (BUTTON_EXPRESS.equals(type)){
            return TYPE_EXPRESS;
        } else if (BUTTON_COPY.equals(type)) {
            return TYPE_COPY;
        } else if (BUTTON_APP.equals(mJumpType)) {
            return TYPE_APP;
        } else if (BUTTON_BROWSER.equals(mJumpType)){
            return TYPE_BROWSER;
        }
        return -1;
    }

    private void sendTimeOutMessage() {
        if (mCodeType == DecodeHandler.RESULT_BAR) {
            mHander.sendEmptyMessageDelayed(MSG_CHECK_BAR_TIMEOUT, TIMEOUT);
        } else {
            mHander.sendEmptyMessageDelayed(MSG_CHECK_QR_TIMEOUT, TIMEOUT);
        }
    }

    private void removeTimeOutMessage() {
        if (mCodeType == DecodeHandler.RESULT_BAR) {
            mHander.removeMessages(MSG_CHECK_BAR_TIMEOUT);
        } else {
            mHander.removeMessages(MSG_CHECK_QR_TIMEOUT);
        }
    }
}
