package com.yunos.camera.ui;

public interface Rotatable {
    // Set parameter 'animation' to true to have animation when rotation.
    public void setOrientation(int orientation, boolean animation);
}
