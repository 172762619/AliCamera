package com.yunos.camera.ui;

import android.content.Context;
import android.content.Intent;
import android.hardware.Camera.Parameters;
import android.view.View;
import android.widget.ImageView;

import com.way.camera.R;
import com.yunos.camera.CameraActivity;
import com.yunos.camera.CameraSettings;
import com.yunos.camera.ComboPreferences;
import com.yunos.camera.Util;
import com.yunos.camera.ui.ModuleIndicatorPanel.AnimatioinCallback;

public class ScanModuleActor extends ModuleActor {
    
    private View mSwitcher;                         // photo, video, scan
    private RotateImageButton mBeautyButton;        // photo, video, scan
    private RotateImageButton mPhotoSettingButton;  // photo, video, scan
    private ImageView mPhotoLines;                  // photo, video, scan

    public ScanModuleActor(Context context, CameraControls controls) {
        super(context, controls);
    }

    @Override
    protected void initializeSpecificControls() {
        mSwitcher = mControls.getSwitcher();
        mBeautyButton = (RotateImageButton) mControls.getEffectsButton();
        mPhotoSettingButton = (RotateImageButton) mControls.getPhotoSetting();
        mPhotoLines = mControls.getPhotoLines();
    }

    @Override
    protected void initializeViews(ComboPreferences pref) {
    }

    @Override
    protected void initializeControlsByIntent(Intent intent, ComboPreferences prefs) {
        if (intent == null) {
            return;
        }
        if (Util.isScanIntent(intent)) {
            String flashmode = CameraSettings.readFlashMode(prefs,
                    CameraActivity.SCANNER_MODULE_INDEX);
            mControls.updateVideoFlashButton(flashmode);
            mPhotoShutter.setVisibility(View.GONE);
            mControls.getVideoShutter().setVisibility(View.GONE);
            mPhotoThumbnailView.setVisibility(View.GONE);
            mControls.getVideoThumbnailView().setVisibility(View.GONE);
            mBeautyButton.setVisibility(View.GONE);
            mSwitcher.setVisibility(View.GONE);
        }
    }

    @Override
    protected void animateToPrevModule(int moduleIndex, ComboPreferences pref, Parameters params,
            AnimatioinCallback callback) {
        // from scan to photo module
        mIndicatorPanel.setAnimationCallback(callback);
        mIndicatorPanel.setSelected(moduleIndex);
        mPhotoShutter.setImageResource(R.drawable.btn_shutter_photo);
        fadeIn(mPhotoShutter);
        fadeIn(mBeautyButton);
        fadeOut(mVideoFlash);
        if(supportFrontFlash(params)){
            fadeIn(mPhotoFlash);
        }
        fadeIn(mSwitcher);
        fadeIn(mPhotoThumbnailView);
        mPhotoSettingButton.setImageResource(R.drawable.ic_camera_setting);
        fadeIn(mPhotoSettingButton);
        fadeIn(mLowerControls);
        mControls.updateEffectsButton(false, false);
    }

    @Override
    protected void animateToNextModule(int moduleIndex, ComboPreferences pref, Parameters params,
            AnimatioinCallback callback) {
        // from scan to pano module
        // Module Indicator
        mIndicatorPanel.setAnimationCallback(callback);
        mIndicatorPanel.setSelected(moduleIndex);
        // Shutter Button
        mPhotoShutter.setImageResource(R.drawable.btn_shutter_panorama);
        fadeIn(mPhotoShutter);
        fadeOut(mVideoFlash);
        fadeIn(mPhotoThumbnailView);
        fadeIn(mLowerControls);
    }

}
