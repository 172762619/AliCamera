
package com.yunos.camera.qrcode;

import a_vcard.android.syncml.pim.PropertyNode;
import a_vcard.android.syncml.pim.VDataBuilder;
import a_vcard.android.syncml.pim.VNode;
import a_vcard.android.syncml.pim.vcard.VCardException;
import a_vcard.android.syncml.pim.vcard.VCardParser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class VcardUtils {
    public static final String VCARD_NAME = "N";
    public static final String VCARD_EMAIL = "EMAIL";
    public static final String VCARD_TEL = "TEL";
    public static final String VCARD_ADR = "ADR";
    public static final String VCARD_ORG = "ORG";
    public static final String VCARD_TITLE = "TITLE";
    public static final String VCARD_URL = "URL";

    public static ArrayList<PropertyNode> getContactInfo(String resultstr) {
        VCardParser parser = new VCardParser();
        VDataBuilder builder = new VDataBuilder();
        boolean parsed = false;
        try {
            parsed = parser.parse(resultstr, "UTF-8", builder);
        } catch (VCardException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!parsed)
            return new ArrayList<PropertyNode>();
        List<VNode> pimContacts = builder.vNodeList;
        return pimContacts.get(0).propList;
    }
}
